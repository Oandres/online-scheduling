# This Julia file contains the nested closed-loop implementation of 
# the integrated optimal control and scheduling case study II: MMA
#
# Scenario: disturbance
#
# The nomenclature may differ from the paper
#
# Oswaldo Andrés-Martínez
# oswxandres@gmail.com
# oandresm@uwaterloo.ca
#
# May 2021

using JuMP, Ipopt, PyPlot, DelimitedFiles, Dierckx

# - Steady states
μp = [43707 54490 76349 96973]
z1p = [5.767 5.83 5.90 5.94]
z2p = [0.216 0.136 0.056 0.024]
z3p = [5.34e-4 3.12e-4 1.29e-4 6.32e-5]
z4p = [23.354 16.996 9.839 6.132]
z5p = [329.91 328.83 327.56 326.95]
z6p = [296.40 296.55 296.62 296.77]
u1p = [0.027 0.017 0.007 0.003]
u2p = [3.6 3.3 3.1 2.9]

# - Desired values for ϕd
ϕd1 = μp[1]
ϕd2 = μp[2]
ϕd3 = μp[3]
ϕd4 = μp[4]

# Model parameters
# - Kinetic parameters
Ztc = 3.8223e10
Ztd = 3.1457e11
Zi  = 3.7920e18
Zp  = 1.7700e9
Zfm = 1.0067e15
fs  = 0.58
Etc = 2.9442e3
Etd = 2.9442e3
Ei  = 1.2877e5
Ep  = 1.8283e4
Efm = 7.4478e4

# - System parameters
z1i0 = 6.4678
z2i0 = 8.0
z5i0 = 350
V  = 0.1
ρ1 = 866
cp = 2.0
A  = 2.0
ρ2 = 1000
V0 = 0.02
R  = 8.314
Mm = 100.12
ΔH = 57800 
U  = 720
cw = 4.2
Tw0 = 293.2
qf = 1      

# Bounds on the controls
u1L = 0.001; u1U = 0.04
u2L = 1.0;   u2U = 4.0

# Disturbance on feed temperature
# - time interval (h) 
τd1 = 33 # from
τd2 = 40 # to
#    magnitud
Md =  -10 

# Scheduling parameters
Dkg = [9550 12220 13220 14150]  # Demand of products (kg)
D = Dkg./ρ1                     # Demand of products (m3)
Cpkg = [0.9 1.2 1.3 1.5]        # Prices of each product ($/kg)
Cp = Cpkg.*ρ1                   # Prices of each product ($/m3)
Cskg = [0.010 0.011 0.012 0.05] # Storage costs ($/kgh)
Cs = Cskg.*ρ1                   # Storage costs ($/m3h)
Cr = 1.5 
Dmax = 1.1D

# Other parameters
δ = 1e3    # Penalty parameter
ϵ = 1e-1   # Tolerance for m(t)

# Off-spec and control effort costs
α1 = 0; α2 = 0; α3 = 0; α4 = 0; α5 = 0; α6 = 0; α7 = 100; α8 = 1

# Tuning parameters for the inner loop
Q1 = 1.0; Q2 = 1.0; Q3 = 1; Q4 = 1.0; Q5 = 1.0; Q6 = 1.0; Q7 = 1.0; Q8 = 1.0; 
R1 = 1.0; R2 = 1.0; R3 = 1; R4 = 1.0; R5 = 1.0; R6 = 1.0;

# Size parameters
# - Number of finite elements
N0 = 16  # scheduling
N1 = 18  # mpc (prediction horizon)
N2 = 1200 # total simulation
N2i = 1  # simulation step
# - Number of collocation points
M = 2    # for scheduling, mpc and simulation
# - Number of products and subintervals
P = 4
Q = 4 

# Collocation matrix and points and quadrature weights 
# (Radau points)
include("Rtableau.jl")
Ω,ω,τ = butcher(M+1)
# Lagrange coefficients for interpolation
include("Lagrange.jl")
lag = Lagrange(0,τ)

# Sets
np = 1:P    # products
nq = 1:Q    # subintervals
n0i = 1:N0  # scheduling
n1i = 1:N1  # mpc
n2i = 1:N2i # simulation step
Ni = Array{UnitRange}(undef,Q)  # Subintervals set
for i in 1:Q
   Ni[i] = ((i-1)*Int(N0/Q) + 1):i*Int(N0/Q)
end
mj = 1:M  # Collocation points
mk = copy(mj)

h1 = Array{Float64}(undef,N1)
h2 = Array{Float64}(undef,N2)
Tf = 80.0  # Approx. final time for simulation
h1 .= Tf/N2
h2 .= Tf/N2

# Position of points sj^t relative to the finite elements
st = [2 2 2 2] 

# Initial values
# - inventory level
ms = [0.0 0.0 0.0 0.0]
msm = copy(ms)

# - Sequence, states and control from open-loop calculations
Seq0 = [1 2 3 4]           # sequence
sw0 = [12.47 28.39 45.61]  # switching instants
z1s = 5.94
z2s = 0.024
z3s = 6.32e-5
z4s = 6.132
z5s = 326.95
z6s = 296.77
u1s = 0.003
u2s = 2.9
zs = [z1s z2s z3s z4s z5s z6s]

# - Dictionary to store the actual switching instants
swr = Dict{Int64,Float64}()
# - Array to store the actual sequence
SEQ = [Seq0[1]]

# Loop Optimization <-> Simulation 
# - Arrays to store the control and state values
#   - Collocation points
uk = Array{Float64}(undef,N2*M,2)
Zk = Array{Float64}(undef,N2*M,6)
Mk = Array{Float64}(undef,N2*M,P)
tk = Array{Float64}(undef,N2)
#   - Non-collocation points
u0k = Array{Float64}(undef,N2,2)
Z0k = Array{Float64}(undef,N2,6)
M0k = Array{Float64}(undef,N2,P)

# Models generation
include("initial_guess.jl")
include("scheduling.jl")
include("mpc.jl")
include("mma_simu.jl")

Ks = [0]
let
   # Initial schedule (previously obtained in open-loop)    
   # - states 
   zsp = [z1p[Seq0[1]] z2p[Seq0[1]] z3p[Seq0[1]] z4p[Seq0[1]] z5p[Seq0[1]] z6p[Seq0[1]];
         z1p[Seq0[2]] z2p[Seq0[2]] z3p[Seq0[2]] z4p[Seq0[2]] z5p[Seq0[2]] z6p[Seq0[2]];
         z1p[Seq0[3]] z2p[Seq0[3]] z3p[Seq0[3]] z4p[Seq0[3]] z5p[Seq0[3]] z6p[Seq0[3]];
         z1p[Seq0[4]] z2p[Seq0[4]] z3p[Seq0[4]] z4p[Seq0[4]] z5p[Seq0[4]] z6p[Seq0[4]]]
   Zsp = zsp[1,:]
   μSP = [μp[Seq0[1]] μp[Seq0[2]] μp[Seq0[3]] μp[Seq0[4]]]
   # - control  
   Usp = [u1p[Seq0[1]] u2p[Seq0[1]];
          u1p[Seq0[2]] u2p[Seq0[2]];
          u1p[Seq0[3]] u2p[Seq0[3]]
          u1p[Seq0[4]] u2p[Seq0[4]]]
   wsp = Usp[1,:] # - dummy  
   μSp = μSP[1] 
   μS = [zs[4]/zs[3]]
   # - switching points
   swt = [sw0[1] sw0[2] sw0[3]]
   # initiation of some variables
   Seqk = copy(Seq0) # Sequence calculated by rescheduling
   ulaw = [u1s u2s]       # Control laws for the nmpc
   ts = [0.0]        # Current simulation time
   tf = [Tf]         # Final time calculated by rescheduling
   prod = collect(np)# Products to be manufactured
   Inv = []          # Products manufactured
   dist  = [true]
   s = [1]           # dummy
   # Loop
   println("Solving, please wait ⏳")
   for ks in 1:N2
      #println("iteration: ",ks)
      Ks[1] = ks
      Id = 1+(ks-1)*M:ks*M  # Index set to save values
      # ========================= Rescheduling ========================= 
      if rem(ks,8) == 0 # rem(ks,freq) where freq is the rescheduling frequency         
         @label again
         set_value(y1in,zs[1])
         set_value(y2in,zs[2])
         set_value(y3in,zs[3])
         set_value(y4in,zs[4])
         set_value(y5in,zs[5])
         set_value(y6in,zs[6])
         set_value(tin,ts[1])
         # Any inventory > Dmax is removed to avoid infeasibility
         for p in np  
            if ms[p] > Dmax[p]
               msm[p] = Dmax[p]
            else
               msm[p] = ms[p]
            end
            set_value(min[p],msm[p])         
         end 
         # New guessed sequence      
         #- If a product was already manufactured, we don't need it in the initial guess        
         if ms[Seq0[1]]>=Dmax[Seq0[1]] 
            for p in 1:(P-1)
               Seq0[p] = Seq0[p+1]
            end

            # - new initial guess values
            include("initial_guess.jl")
            for i in n0i,j in mj
               set_start_value(y1[i,j],x10[i,j])
               set_start_value(y2[i,j],x20[i,j])
               set_start_value(y3[i,j],x30[i,j])
               set_start_value(y4[i,j],x40[i,j])
               set_start_value(y5[i,j],x50[i,j])
               set_start_value(y6[i,j],x60[i,j])
               set_start_value(w1[i,j],U10[i,j])
               set_start_value(w2[i,j],U20[i,j])
               set_start_value(tt[i,j],ts[1])
            end
            for i in n0i
               set_start_value(y10[i],x10[i,1])
               set_start_value(y20[i],x20[i,1])
               set_start_value(y30[i],x30[i,1])
               set_start_value(y40[i],x40[i,1])
               set_start_value(y50[i],x50[i,1])
               set_start_value(y60[i],x60[i,1])
               set_start_value(tt0[i],ts[1])
            end
            for p in np,i in n0i,j in mj
               set_start_value(m[p,i,j],M0[p,i,j])
            end
            for p in np,i in n0i
               set_start_value(m0[p,i],M0[p,i,1])
            end
            for p in np,q in nq
               set_start_value(y[p,q],y0[p,q])
            end
         end
         # Intervals that are no longer necessary are removed (v = 0)
         # The corresponding finite elements are fixed to an arbitrary value
         # This helps to speed up the calculations
         r = [0]
         for p in Inv
            if p in Seq0
               continue
            else
               for q in nq
                  fix(y[p,q],-1.0,force=true)
               end
               r[1] = r[1]+1
               fix(v[Q+1-r[1]],0.0,force=true)
               for i in Ni[Q+1-r[1]]
                  fix(h0[i],0.25,force=true)
               end
            end
         end
         optimize!(sch)
         status_sch = termination_status(sch)         
         if Int(status_sch) != 1 && Int(status_sch) != 4 && Int(status_sch) != 10
            # If for some reason the scheduling breaks, 
            # the mpc will continue with the previous schedule
            println("⛔ rescheduling broke at iteration: ",ks)
            println("the mpc will continue with the previous schedule")
            @goto cont   
         else            
            if value(Jpen) >= 1e-3
               # If Jpen is too big, the sequence is not well defined, a subroutine to update δ
               # may be necessary. For now, the mpc will continue with the previous schedule 
               println("⛔ Penalty term became too big at iteration: ",ks)
               println("the mpc will continue with the previous schedule")
               @goto cont              
            end
            # - reading the scheduling results
            ys = transpose(value.(y).data)[:]
            y3sn = transpose(value.(y3).data)[:]
            y4sn = transpose(value.(y4).data)[:]
            μsn = y4sn ./ y3sn
            tts  = transpose(value.(tt).data)[:]
            tsw1 = value(tt[Ni[1][end],M])
            tsw2 = value(tt[Ni[2][end],M])
            tsw3 = value(tt[Ni[3][end],M])
            vs = value.(v).data
            # - updating sequence and set points 
            # - note that the y's may not be exactly 1
            for q in nq,p in np
               if ys[q+(p-1)*P] >= 0.999 && ys[q+(p-1)*P] <= 1.001
                  Seqk[q]  = p
                  zsp[q,:] = [z1p[p] z2p[p] z3p[p] z4p[p] z5p[p] z6p[p]]
                  Usp[q,:] = [u1p[p] u2p[p]]
                  μSP[q] = μp[p]
               end
            end
            # - updating switching points and final time
            swt = [tsw1 tsw2 tsw3]
            tf[1] = value(tt[N0,M])
            # - updatinig setpoints for the MPC           
            Zsp = zsp[1,:]
            wsp = Usp[1,:]
            μSp = μSP[1]
            # - the number of switching points is restarted            
            s[1] = 1
         end
      end 
      # =======================================================
      # Switching to the next setpoint
      @label cont
      if s[1] <= length(swt) 
         if ts[1] >= swt[s[1]]
            swr[ks] = ts[1]
            Zsp = zsp[s[1]+1,:]
            wsp = Usp[s[1]+1,:]
            μSp = μSP[s[1]+1]
            push!(SEQ,Seqk[s[1]+1])
            s[1] += 1
         end
      end
      # =================== (N)MPC ===================== 
      # If ks = 1, mpc is not executed, so we use the initial us
      if ks > 1
         # - initial values
         set_value(z1in,zs[1])
         set_value(z2in,zs[2])
         set_value(z3in,zs[3])
         set_value(z4in,zs[4])
         set_value(z5in,zs[5])
         set_value(z6in,zs[6])
         # - set-points
         set_value(z1sp,Zsp[1])
         set_value(z2sp,Zsp[2])
         set_value(μsp,μSp)
         set_value(z5sp,Zsp[5])
         set_value(z6sp,Zsp[6])
         set_value(u1sp,wsp[1])
         set_value(u2sp,wsp[2])
         # - initial guess values
         for i in n1i
            for j in mj
               set_start_value(z1[i,j],Zsp[1])
               set_start_value(z2[i,j],Zsp[2])
               set_start_value(z3[i,j],Zsp[3])
               set_start_value(z4[i,j],Zsp[4])
               set_start_value(z5[i,j],Zsp[5])
               set_start_value(z6[i,j],Zsp[6])
               set_start_value(u1[i,j],wsp[1])
               set_start_value(u2[i,j],wsp[2])
            end
         end
         optimize!(mpc)
         status_mpc = termination_status(mpc)
         if Int(status_mpc) != 1 && Int(status_mpc) != 4 && Int(status_mpc) != 10
            println("⛔ mpc broke at iteration:",ks)
            break
         end
         # The u law is retrieved
         ulaw[1] = value(u1[1,1])
         ulaw[2] = value(u2[1,1])
         uk[Id,1] .= ulaw[1]   
         uk[Id,2] .= ulaw[2]
      else
         uk[Id,1] .= ulaw[1]   
         uk[Id,2] .= ulaw[2] 
      end
      # ================================================ 
      # =================== Simulation ===================== 
      for j in mj
         set_value(U1[1,j],ulaw[1])
         set_value(U2[1,j],ulaw[2])
      end
      # Initial values
      set_value(Z1in,zs[1])
      set_value(Z2in,zs[2])  
      set_value(Z3in,zs[3])
      set_value(Z4in,zs[4]) 
      set_value(Z5in,zs[5])
      set_value(Z6in,zs[6]) 
      for p in np
         set_value(Mi[p],ms[p])
      end
      # Disturbance in zi0
      if ts[1] >= τd1 && dist[1] == true
         set_value(Z5i0,z5i0+Md)
      end
      if ts[1] > τd2 && dist[1] == true
         set_value(Z5i0,z5i0)
         dist[1] = false
      end 
      optimize!(mma)
      status_simu = termination_status(mma)         
      if Int(status_simu) != 1 && Int(status_simu) != 4 && Int(status_simu) != 10
         println(status_simu)
         println("⛔ simulation broke at iteration: ",ks)         
         break
      end
      # Reading simulation results
      Zk[Id,1]  = transpose(value.(Z1).data)[:]
      Z0k[ks,1] = value.(Z10).data[1]      
      Zk[Id,2]  = transpose(value.(Z2).data)[:]
      Z0k[ks,2] = value.(Z20).data[1]
      Zk[Id,3]  = transpose(value.(Z3).data)[:]
      Z0k[ks,3] = value.(Z30).data[1]      
      Zk[Id,4]  = transpose(value.(Z4).data)[:]
      Z0k[ks,4] = value.(Z40).data[1]
      Zk[Id,5]  = transpose(value.(Z5).data)[:]
      Z0k[ks,5] = value.(Z50).data[1]      
      Zk[Id,6]  = transpose(value.(Z6).data)[:]
      Z0k[ks,6] = value.(Z60).data[1]

      Mk[Id,1]  = transpose(value.(M1).data)[:]
      M0k[ks,1] = value.(M10).data[1]
      Mk[Id,2]  = transpose(value.(M2).data)[:]
      M0k[ks,2] = value.(M20).data[1]
      Mk[Id,3]  = transpose(value.(M3).data)[:]
      M0k[ks,3] = value.(M30).data[1]
      Mk[Id,4]  = transpose(value.(M4).data)[:]
      M0k[ks,4] = value.(M40).data[1]
      ts[1] = h2[ks]*ks      
      zs[1] = Zk[ks*M,1]
      zs[2] = Zk[ks*M,2]
      zs[3] = Zk[ks*M,3]
      zs[4] = Zk[ks*M,4]
      zs[5] = Zk[ks*M,5]
      zs[6] = Zk[ks*M,6]
      ms[1] = Mk[ks*M,1]
      ms[2] = Mk[ks*M,2]
      ms[3] = Mk[ks*M,3]
      ms[4] = Mk[ks*M,4]
      μS[1] = zs[4]/zs[3]
      # ================================================
      tk[ks] = ts[1] # Save time
      for p in prod
         if ms[p] >= D[p]
            setdiff!(prod,[p])
            push!(Inv,p)
            print("Manufactured products: ")
            writedlm(stdout,Inv')
         end
      end

      if ts[1] >= tf[1]
         println("Final time reached ✅")
         if isempty(prod)
            println("Demands were fulfilled ✅")
         else
            println("Demands were not fulfilled ⛔")
         end 
         break
      end
   end
end

# Final results
# - Subinterval lengths
nsw = length(swr) # number of switching points
nps = length(SEQ) # number of production subintervals
tf  = tk[Ks[1]]   # real final time

tp = Array{Float64}(undef,nps)
Swr = sort(collect(swr),by=x->x[1])
# - First 
tp[1] = Swr[1][2] 
# - Those in the middle
for j in 1:(nsw-1)
   tp[j+1] = Swr[j+1][2]-Swr[j][2]
end
# - Last 
tp[end] = tf - Swr[end][2]   

# - Inventory levels at each switching point
#   (in the order they were produced)
inv = Array{Float64}(undef,nps)
for i in 1:nps-1
   inv[i] = Mk[(Swr[i][1])*M,SEQ[i]]
end
inv[end] = Mk[Ks[1]*M,SEQ[end]]

# - Calculation of inventory costs
#    (note that the last product is not stored)
ic = Array{Float64}(undef,nps-1)
for i in 1:nps-1
   ic[i] = inv[i]*sum(tp[i+1:end])*Cs[SEQ[i]]
end

# - Sales
sc = Array{Float64}(undef,P)
for i in np
   sc[i] = Mk[Ks[1]*M,i]*Cp[i]
end   

# - Dynamic cost
#   Index set for subintervals
swf = Array{UnitRange}(undef,nps) 
swf[1] = 1:Swr[1][1]
for i in 2:nps-1
   swf[i] = Swr[i-1][1]:Swr[i][1]
end
swf[end] = Swr[end][1]:Ks[1]
#   dynamic cost for each subinterval
dc = Array{Float64}(undef,nps)
for p in 1:nps
   for i in swf[p]
      for j in mj
         dc[p] = dc[p] + h2[i]*ω[j]*(α1*(z1p[SEQ[p]]-Zk[j+M*(i-1),1])^2  
               + α2*(z2p[SEQ[p]]-Zk[j+M*(i-1),2])^2 + α3*(z3p[SEQ[p]]-Zk[j+M*(i-1),3])^2 
               + α4*(z4p[SEQ[p]]-Zk[j+M*(i-1),4])^2 + α5*(z5p[SEQ[p]]-Zk[j+M*(i-1),5])^2
               + α6*(z6p[SEQ[p]]-Zk[j+M*(i-1),6])^2 + α7*(u1p[SEQ[p]]-uk[j+M*(i-1),1])^2
               + α8*(u2p[SEQ[p]]-uk[j+M*(i-1),2])^2)
      end
   end
end

# - Total cost
TC = sum(sc) - sum(ic) - qf*Cr*tf - sum(dc)

# Plots in the domain t  
include("plots_t.jl")