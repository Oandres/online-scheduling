# This Julia file contains the model of the integrated optimal control
# and scheduling case study II: MMA
#
# The nomenclature may differ from the paper
#
# Oswaldo Andrés-Martínez
# oswxandres@gmail.com
# oandresm@uwaterloo.ca
#
# May 2021  

# Model definition
sch = Model(Ipopt.Optimizer)

# Variables
@variable(sch,y1[i in n0i,j in mj],start=x10[i,j]) 
@variable(sch,0.001<=y2[i in n0i,j in mj]<=0.5,start=x20[i,j])   
@variable(sch,1e-5<=y3[i in n0i,j in mj]<=1e-3,start=x30[i,j])              
@variable(sch,1.0<=y4[i in n0i,j in mj]<=50,start=x40[i,j]) 
@variable(sch,y5[i in n0i,j in mj],start=x50[i,j])   
@variable(sch,y6[i in n0i,j in mj],start=x60[i,j]) 
@variable(sch,y7[i in n0i,j in mj])
@variable(sch,0<=v[q in nq]<=40,start=10)
@variable(sch,0<=m[p in np,i in n0i,j in mj]<= Dmax[p],start=M0[p,i,j])
@variable(sch,y10[i in n0i],start=x10[i,1])
@variable(sch,0.001<=y20[i in n0i]<=0.5,start=x20[i,1])
@variable(sch,1e-5<=y30[i in n0i]<=1e-3,start=x30[i,1])
@variable(sch,1.0<=y40[i in n0i]<=50,start=x40[i,1])
@variable(sch,y50[i in n0i],start=x50[i,1])
@variable(sch,y60[i in n0i],start=x60[i,1])
@variable(sch,y70[i in n0i])
@variable(sch,-1<=y[p in np,q in nq]<=1,start=y0[p,q])
@variable(sch,m0[p in np,i in n0i]>=0,start=M0[p,i,1])
@variable(sch,tt[i in n0i,j in mj]>=0)
@variable(sch,tt0[i in n0i]>=0)
@variable(sch,u1L <= w1[i in n0i,j in mj] <= u1U,start=U10[i,j])
@variable(sch,u2L <= w2[i in n0i,j in mj] <= u2U,start=U20[i,j])
@variable(sch,0.05Q/N0 <= h0[i in n0i] <= 2Q/N0,start=Q/N0)

# Parameters
@NLparameter(sch,y1in == zs[1])
@NLparameter(sch,y2in == zs[2])
@NLparameter(sch,y3in == zs[3])
@NLparameter(sch,y4in == zs[4])
@NLparameter(sch,y5in == zs[5])
@NLparameter(sch,y6in == zs[6])
@NLparameter(sch,y7in == 0)
@NLparameter(sch,tin  == 0)
@NLparameter(sch,min[p in np] == ms[p])

# ================  Auxiliary expressions ============
# - Penalty function
function γp(x)
   return log(0.5,x)
end
register(sch,:γp,1,γp,autodiff=true)


# - MWD
@NLexpression(sch,μb[i in n0i,j in mj],y4[i,j]/y3[i,j])

# - P0
@NLexpression(sch,P0[i in n0i,j in mj],
   (2*fs*y2[i,j]*Zi*exp(-Ei/R/y5[i,j])/(Ztd*exp(-Etd/R/y5[i,j])
                                     +Ztc*exp(-Etc/R/y5[i,j])))^(0.5))

# - Deviation from the steady state
@NLexpression(sch,ss[p in np,i in n0i,j in mj],α1*(z1p[p]-y1[i,j])^2 
  + α2*(z2p[p]-y2[i,j])^2 + α3*(z3p[p]-y3[i,j])^2 + α4*(z4p[p]-y4[i,j])^2 
  + α5*(z5p[p]-y5[i,j])^2 + α6*(z6p[p]-y6[i,j])^2 + α7*(u1p[p]-w1[i,j])^2 
                                                  + α8*(u2p[p]-w2[i,j])^2)

# - RHS of the state equations
@NLexpression(sch,r1[i in n0i,j in mj],
 -(Zp*exp(-Ep/R/y5[i,j])+Zfm*exp(-Efm/R/y5[i,j]))*y1[i,j]*P0[i,j] + qf*(z1i0-y1[i,j])/V)
   
@NLexpression(sch,r2[i in n0i,j in mj],-Zi*exp(-Ei/R/y5[i,j])*y2[i,j] 
                                       + (w1[i,j]*z2i0-qf*y2[i,j])/V)

@NLexpression(sch,r3[i in n0i,j in mj],
   (0.5*Ztc*exp(-Etc/R/y5[i,j])+Ztd*exp(-Etd/R/y5[i,j]))*P0[i,j]^2 
      + Zfm*exp(-Efm/R/y5[i,j])*y1[i,j]*P0[i,j] - qf*y3[i,j]/V)

@NLexpression(sch,r4[i in n0i,j in mj],
   Mm*(Zp*exp(-Ep/R/y5[i,j])+Zfm*exp(-Efm/R/y5[i,j]))*y1[i,j]*P0[i,j] - qf*y4[i,j]/V)

@NLexpression(sch,r5[i in n0i,j in mj],P0[i,j]*Zp*exp(-Ep/R/y5[i,j])*y1[i,j]*ΔH/ρ1/cp
   - U*A*(y5[i,j]-y6[i,j])/ρ1/cp/V + qf*(z5i0 - y5[i,j])/V)

@NLexpression(sch,r6[i in n0i,j in mj],w2[i,j]*(Tw0-y6[i,j])/V0 
                                            + U*A*(y5[i,j]-y6[i,j])/ρ2/cw/V0)

@NLexpression(sch,r7[q in nq,i in Ni[q],j in mj],
                sum(((y[p,q]+1)/2)*ss[p,i,j] for p in np))

@NLexpression(sch,r8[p in np,q in nq,i in n0i,j in mj],(v[q]*(y[p,q]+1)/2)*qf)

# - Interpolation of algebraic variables
@NLexpression(sch,w10[i in n0i],sum(lag[j]*w1[i,j] for j in mj))
@NLexpression(sch,w20[i in n0i],sum(lag[j]*w2[i,j] for j in mj))


# - Collocation equations
#   RHS of the state equations in the domain s
@NLexpression(sch,y1dot[q in nq,i in Ni[q],j in mj],v[q]*r1[i,j])
@NLexpression(sch,y2dot[q in nq,i in Ni[q],j in mj],v[q]*r2[i,j])
@NLexpression(sch,y3dot[q in nq,i in Ni[q],j in mj],v[q]*r3[i,j])
@NLexpression(sch,y4dot[q in nq,i in Ni[q],j in mj],v[q]*r4[i,j])
@NLexpression(sch,y5dot[q in nq,i in Ni[q],j in mj],v[q]*r5[i,j])
@NLexpression(sch,y6dot[q in nq,i in Ni[q],j in mj],v[q]*r6[i,j])
@NLexpression(sch,y7dot[q in nq,i in Ni[q],j in mj],v[q]*r7[q,i,j])
@NLexpression(sch,mdot1[p in np,q in nq,i in Ni[q],j in mj;i<Ni[q][st[q]]+1],0)
@NLexpression(sch,mdot2[p in np,q in nq,i in Ni[q],j in mj;i>=Ni[q][st[q]]+1],r8[p,q,i,j])
#   Equation for t(s)
@NLexpression(sch,tdot[q in nq,i in Ni[q],j in mj],v[q])

# - Economic expresion for the objective function
@NLexpression(sch,Jp[q in nq,i in Ni[q],j in mj],sum(δ*γp(y[p,q]^2) for p in np))
@NLexpression(sch,Jpint[q in nq],sum(h0[i]*ω[j]*v[q]*Jp[q,i,j] for i in Ni[q],j in mj))
@NLexpression(sch,Jpen,sum(Jpint[q] for q in nq))

@NLexpression(sch,Jsch,sum(Cp[p]*m[p,N0,M] for p in np) - qf*Cr*tt[N0,M]
   - sum(Cs[p]*m[p,N0,M]*((y[p,q]+1)/2)*(tt[N0,M]-tt[Ni[q][end],M]) 
                for p in np,q in nq))

@NLexpression(sch,Jdyn,sum(y7[Ni[q][end],M] for q in nq))
# =====================================================================


# Objective function
@NLobjective(sch,Max,Jsch - Jdyn - Jpen)

# Constraints
# - RK representation of the state and adjoint equations
@NLconstraint(sch,G1[q in nq,i in Ni[q],j in mj],
      y1[i,j] == y10[i]+h0[i]*sum(Ω[j,k]*y1dot[q,i,k] for k in mk))
@NLconstraint(sch,G2[q in nq,i in Ni[q],j in mj],
      y2[i,j] == y20[i]+h0[i]*sum(Ω[j,k]*y2dot[q,i,k] for k in mk))
@NLconstraint(sch,G3[q in nq,i in Ni[q],j in mj],
      y3[i,j] == y30[i]+h0[i]*sum(Ω[j,k]*y3dot[q,i,k] for k in mk))
@NLconstraint(sch,G4[q in nq,i in Ni[q],j in mj],
      y4[i,j] == y40[i]+h0[i]*sum(Ω[j,k]*y4dot[q,i,k] for k in mk))
@NLconstraint(sch,G5[q in nq,i in Ni[q],j in mj],
      y5[i,j] == y50[i]+h0[i]*sum(Ω[j,k]*y5dot[q,i,k] for k in mk))
@NLconstraint(sch,G6[q in nq,i in Ni[q],j in mj],
      y6[i,j] == y60[i]+h0[i]*sum(Ω[j,k]*y6dot[q,i,k] for k in mk))
@NLconstraint(sch,G7[q in nq,i in Ni[q],j in mj],
      y7[i,j] == y70[i]+h0[i]*sum(Ω[j,k]*y7dot[q,i,k] for k in mk))
@NLconstraint(sch,G81[p in np,q in nq,i in Ni[q],j in mj;i<Ni[q][st[q]]+1],
      m[p,i,j] == m0[p,i]+h0[i]*sum(Ω[j,k]*mdot1[p,q,i,k] for k in mk))
@NLconstraint(sch,G82[p in np,q in nq,i in Ni[q],j in mj;i>=Ni[q][st[q]]+1],
      m[p,i,j] == m0[p,i]+h0[i]*sum(Ω[j,k]*mdot2[p,q,i,k] for k in mk))
@NLconstraint(sch,G9[q in nq,i in Ni[q],j in mj],
      tt[i,j] == tt0[i]+h0[i]*sum(Ω[j,k]*tdot[q,i,k] for k in mk))

# - Continuity
@NLconstraint(sch,C1[i in 2:N0],y10[i] == y1[i-1,M])
@NLconstraint(sch,C2[i in 2:N0],y20[i] == y2[i-1,M])
@NLconstraint(sch,C3[i in 2:N0],y30[i] == y3[i-1,M])
@NLconstraint(sch,C4[i in 2:N0],y40[i] == y4[i-1,M])
@NLconstraint(sch,C5[i in 2:N0],y50[i] == y5[i-1,M])
@NLconstraint(sch,C6[i in 2:N0],y60[i] == y6[i-1,M])
@NLconstraint(sch,C7[i in 2:N0],y70[i] == y7[i-1,M])
@NLconstraint(sch,C8[p in np,i in 2:N0],m0[p,i] == m[p,i-1,M])
@NLconstraint(sch,C9[i in 2:N0],tt0[i] == tt[i-1,M])

# - Boundary values
@NLconstraint(sch,y1iv,y10[1] == y1in)
@NLconstraint(sch,y2iv,y20[1] == y2in)
@NLconstraint(sch,y3iv,y30[1] == y3in)
@NLconstraint(sch,y4iv,y40[1] == y4in)
@NLconstraint(sch,y5iv,y50[1] == y5in)
@NLconstraint(sch,y6iv,y60[1] == y6in)
@NLconstraint(sch,y7iv,y70[1] == y7in)
@NLconstraint(sch,ttiv,tt0[1] == tin)
@NLconstraint(sch,miv[p in np],m0[p,1] == min[p])

# - Demand constraint
@NLconstraint(sch,mfv[q in nq],sum(((y[p,q]+1)/2)*m[p,Ni[q][end],M] for p in np) >= 
                     sum(((y[p,q]+1)/2)*D[p] for p in np))

# - Transition must end at the end of st[q] element
@NLconstraint(sch,μbtr[q in nq],μb[Ni[q][st[q]],M] == 
                                    sum(((y[p,q]+1)/2)*μp[p] for p in np))

# - Then, the state is forced to remain at this value along the rest of subinterval q
@NLconstraint(sch,μtc[q in nq,i in Ni[q],j in mj;i>=Ni[q][st[q]]+1],μb[i,j] ==
                                                                  μb[Ni[q][st[q]],M])

# - Low order representation for u
@NLconstraint(sch,w1c[q in nq,i in Ni[q][1]:(Ni[q][st[q]]+1),j in mj;j!=1],
                                                            w1[i,1] == w1[i,j])
@NLconstraint(sch,w2c[q in nq,i in Ni[q][1]:(Ni[q][st[q]]+1),j in mj;j!=1],
                                                            w2[i,1] == w2[i,j])

# - Assignment constraints
@NLconstraint(sch,ass1[q in nq],sum(y[p,q] for p in np) == 2 - P)

# - Summation of hi's
@NLconstraint(sch,sumh0[q in nq],sum(h0[i] for i in Ni[q]) == 1)
