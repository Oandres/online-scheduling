This folder contains the Julia files to run a case study from the paper: A nested online scheduling and nonlinear model predictive control framework for multi-product continuous systems - Oswaldo Andrés-Martínez and Luis A. Ricardez- Sandoval

Packages: JuMP, Ipopt, PyPlot, Jacobi, SparseArrays

If solver MA27 is not available, ipopt.opt file can be removed so that the default linear solver is used, though the results and CPU times may be somewhat different from those in the paper.

To run the programs, it is necessary only to run the file mma_main.jl
The code was tested in Julia 1.6.3.
