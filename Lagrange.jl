# This functions generates the Lagrange polynomial coefficients for a given x
# and a given node set xi =[x1, x2,...]'
function Lagrange(x,xi)
	n = size(xi,1)
	p = Array{Float64}(undef,1,n)
	
	for i in 1:n
		p[i] = 1
		for j in 1:n
			if j==i
				continue
			end
			
			p[i] *= (x-xi[j])/(xi[i]-xi[j])
		end
	end
	return p
end
