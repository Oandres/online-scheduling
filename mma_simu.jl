# This Julia file contains the model of the MMA reactor
#
# The nomenclature may differ from the paper
#
# Oswaldo Andrés-Martínez
# oswxandres@gmail.com
# oandresm@uwaterloo.ca
#
# May 2021

# Model definition
mma = Model(Ipopt.Optimizer)

# Variables
@variable(mma,Z1[i in n2i,j in mj],start=zs[1]) 
@variable(mma,Z2[i in n2i,j in mj],start=zs[2])       
@variable(mma,Z3[i in n2i,j in mj],start=zs[3])
@variable(mma,Z4[i in n2i,j in mj],start=zs[4])
@variable(mma,Z5[i in n2i,j in mj],start=zs[5])
@variable(mma,Z6[i in n2i,j in mj],start=zs[6])
@variable(mma,M1[i in n2i,j in mj],start=ms[1])  
@variable(mma,M2[i in n2i,j in mj],start=ms[2]) 
@variable(mma,M3[i in n2i,j in mj],start=ms[3]) 
@variable(mma,M4[i in n2i,j in mj],start=ms[4]) 
@variable(mma,Z10[i in n2i],start=zs[1])
@variable(mma,Z20[i in n2i],start=zs[2])
@variable(mma,Z30[i in n2i],start=zs[3])
@variable(mma,Z40[i in n2i],start=zs[4])
@variable(mma,Z50[i in n2i],start=zs[5])
@variable(mma,Z60[i in n2i],start=zs[6])
@variable(mma,M10[i in n2i],start=ms[1])
@variable(mma,M20[i in n2i],start=ms[2])
@variable(mma,M30[i in n2i],start=ms[3])
@variable(mma,M40[i in n2i],start=ms[4])

# Parameters
# - initial states, control and inventory are treated as parameters, 
#   they are initialized with z(t_0), u(t_0), m(t_0)
@NLparameter(mma,U1[i in n2i,j in mj] == 0.01)
@NLparameter(mma,U2[i in n2i,j in mj] == 2.0)
@NLparameter(mma,Z1in == z1s)
@NLparameter(mma,Z2in == z2s)
@NLparameter(mma,Z3in == z3s)
@NLparameter(mma,Z4in == z4s)
@NLparameter(mma,Z5in == z5s)
@NLparameter(mma,Z6in == z6s)
@NLparameter(mma,Mi[p in np] == ms[p])

# Parameter for disturbance
@NLparameter(mma,Z5i0 == z5i0)

# ================  Auxiliary expressions ============
# - δ functions
function δ1(x)
   if x <= ϕd1*(1+ϵ) && x >= ϕd1*(1-ϵ)
      return 1.0
   else
      return 0.0
   end
end
register(mma,:δ1,1,δ1,autodiff=true)

function δ2(x)
   if x <= ϕd2*(1+ϵ) && x >= ϕd2*(1-ϵ)
      return 1.0
   else
      return 0.0
   end
end
register(mma,:δ2,1,δ2,autodiff=true)

function δ3(x)
   if x <= ϕd3*(1+ϵ) && x >= ϕd3*(1-ϵ)
      return 1.0
   else
      return 0.0
   end
end
register(mma,:δ3,1,δ3,autodiff=true)

function δ4(x)
   if x <= ϕd4*(1+ϵ) && x >= ϕd4*(1-ϵ)
      return 1.0
   else
      return 0.0
   end
end
register(mma,:δ4,1,δ4,autodiff=true)

# - Quality function (MWD)
@NLexpression(mma,μb[i in n2i,j in mj],Z4[i,j]/Z3[i,j])

# - P0
@NLexpression(mma,P0[i in n2i,j in mj],
   (2*fs*Z2[i,j]*Zi*exp(-Ei/R/Z5[i,j])/(Ztd*exp(-Etd/R/Z5[i,j])
                                     +Ztc*exp(-Etc/R/Z5[i,j])))^(0.5))

# - RHS of the state equations
@NLexpression(mma,F1[i in n2i,j in mj],
 -(Zp*exp(-Ep/R/Z5[i,j])+Zfm*exp(-Efm/R/Z5[i,j]))*Z1[i,j]*P0[i,j] + qf*(z1i0-Z1[i,j])/V)

@NLexpression(mma,F2[i in n2i,j in mj],-Zi*exp(-Ei/R/Z5[i,j])*Z2[i,j] 
                                                + (U1[i,j]*z2i0-qf*Z2[i,j])/V)

@NLexpression(mma,F3[i in n2i,j in mj],
   (0.5*Ztc*exp(-Etc/R/Z5[i,j])+Ztd*exp(-Etd/R/Z5[i,j]))*P0[i,j]^2 
                      + Zfm*exp(-Efm/R/Z5[i,j])*Z1[i,j]*P0[i,j] - qf*Z3[i,j]/V)

@NLexpression(mma,F4[i in n2i,j in mj],
   Mm*(Zp*exp(-Ep/R/Z5[i,j])+Zfm*exp(-Efm/R/Z5[i,j]))*Z1[i,j]*P0[i,j] - qf*Z4[i,j]/V)

@NLexpression(mma,F5[i in n2i,j in mj],P0[i,j]*Zp*exp(-Ep/R/Z5[i,j])*Z1[i,j]*ΔH/ρ1/cp
   - U*A*(Z5[i,j]-Z6[i,j])/ρ1/cp/V + qf*(Z5i0 - Z5[i,j])/V)

@NLexpression(mma,F6[i in n2i,j in mj],U2[i,j]*(Tw0-Z6[i,j])/V0 
                                            + U*A*(Z5[i,j]-Z6[i,j])/ρ2/cw/V0)

@NLexpression(mma,F71[i in n2i,j in mj],qf*δ1(μb[i,j]))
@NLexpression(mma,F72[i in n2i,j in mj],qf*δ2(μb[i,j]))
@NLexpression(mma,F73[i in n2i,j in mj],qf*δ3(μb[i,j]))
@NLexpression(mma,F74[i in n2i,j in mj],qf*δ4(μb[i,j]))


# - Collocation equations
#   RHS of the state equations 
@NLexpression(mma,Z1dot[i in n2i,j in mj],F1[i,j])
@NLexpression(mma,Z2dot[i in n2i,j in mj],F2[i,j])
@NLexpression(mma,Z3dot[i in n2i,j in mj],F3[i,j])
@NLexpression(mma,Z4dot[i in n2i,j in mj],F4[i,j])
@NLexpression(mma,Z5dot[i in n2i,j in mj],F5[i,j])
@NLexpression(mma,Z6dot[i in n2i,j in mj],F6[i,j])
@NLexpression(mma,Mdot1[i in n2i,j in mj],F71[i,j])
@NLexpression(mma,Mdot2[i in n2i,j in mj],F72[i,j])
@NLexpression(mma,Mdot3[i in n2i,j in mj],F73[i,j])
@NLexpression(mma,Mdot4[i in n2i,j in mj],F74[i,j])
# =====================================================================

# Objective function (dummy)
@NLobjective(mma,Min,1)

# Constraints
# - RK representation of the state and adjoint equations
@NLconstraint(mma,H1[i in n2i,j in mj],
      Z1[i,j] == Z10[i]+h2[i]*sum(Ω[j,k]*Z1dot[i,k] for k in mk))
@NLconstraint(mma,H2[i in n2i,j in mj],
      Z2[i,j] == Z20[i]+h2[i]*sum(Ω[j,k]*Z2dot[i,k] for k in mk))
@NLconstraint(mma,H3[i in n2i,j in mj],
      Z3[i,j] == Z30[i]+h2[i]*sum(Ω[j,k]*Z3dot[i,k] for k in mk))
@NLconstraint(mma,H4[i in n2i,j in mj],
      Z4[i,j] == Z40[i]+h2[i]*sum(Ω[j,k]*Z4dot[i,k] for k in mk))
@NLconstraint(mma,H5[i in n2i,j in mj],
      Z5[i,j] == Z50[i]+h2[i]*sum(Ω[j,k]*Z5dot[i,k] for k in mk))
@NLconstraint(mma,H6[i in n2i,j in mj],
      Z6[i,j] == Z60[i]+h2[i]*sum(Ω[j,k]*Z6dot[i,k] for k in mk))
@NLconstraint(mma,H71[i in n2i,j in mj],
      M1[i,j] == M10[i]+h2[i]*sum(Ω[j,k]*Mdot1[i,k] for k in mk))
@NLconstraint(mma,H72[i in n2i,j in mj],
      M2[i,j] == M20[i]+h2[i]*sum(Ω[j,k]*Mdot2[i,k] for k in mk))
@NLconstraint(mma,H73[i in n2i,j in mj],
      M3[i,j] == M30[i]+h2[i]*sum(Ω[j,k]*Mdot3[i,k] for k in mk))
@NLconstraint(mma,H74[i in n2i,j in mj],
      M4[i,j] == M40[i]+h2[i]*sum(Ω[j,k]*Mdot4[i,k] for k in mk))


# - Initial values
@NLconstraint(mma,Z1iv,Z10[1] == Z1in)
@NLconstraint(mma,Z2iv,Z20[1] == Z2in)
@NLconstraint(mma,Z3iv,Z30[1] == Z3in)
@NLconstraint(mma,Z4iv,Z40[1] == Z4in)
@NLconstraint(mma,Z5iv,Z50[1] == Z5in)
@NLconstraint(mma,Z6iv,Z60[1] == Z6in)
@NLconstraint(mma,M1iv,M10[1] == Mi[1])
@NLconstraint(mma,M2iv,M20[1] == Mi[2])
@NLconstraint(mma,M3iv,M30[1] == Mi[3])
@NLconstraint(mma,M4iv,M40[1] == Mi[4])