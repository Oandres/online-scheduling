# This Julia file contains the mpc of the integrated optimal control
# and scheduling case study II: MMA
#
# The nomenclature may differ from the paper
#
# Oswaldo Andrés-Martínez
# oswxandres@gmail.com
# oandresm@uwaterloo.ca
# 
# May 2021

using JuMP, Ipopt, PyPlot

# Model definition
mpc = Model(Ipopt.Optimizer)

# Variables
@variable(mpc,z1[i in n1i,j in mj],start=z1s) 
@variable(mpc,0.001<=z2[i in n1i,j in mj]<=0.5,start=z2s)
@variable(mpc,1e-5<=z3[i in n1i,j in mj]<=1e-3,start=z3s) 
@variable(mpc,1.0<=z4[i in n1i,j in mj]<=50,start=z4s)
@variable(mpc,z5[i in n1i,j in mj],start=z5s) 
@variable(mpc,z6[i in n1i,j in mj],start=z6s)             
@variable(mpc,z10[i in n1i],start=z1s)
@variable(mpc,0.001<=z20[i in n1i]<=0.5,start=z2s)
@variable(mpc,1e-5<=z30[i in n1i]<=1e-3,start=z3s)
@variable(mpc,1.0<=z40[i in n1i]<=50,start=z4s)
@variable(mpc,z50[i in n1i],start=z5s)
@variable(mpc,z60[i in n1i],start=z6s)
@variable(mpc,u1L <= u1[i in n1i,j in mj] <= u1U,start=u1s)
@variable(mpc,u2L <= u2[i in n1i,j in mj] <= u2U,start=u2s)

# Parameters
# - initial state and control are treated as parameters, 
#   they are initialized with z(t_0) and u(t_0)
@NLparameter(mpc,z1in == zs[1])
@NLparameter(mpc,z2in == zs[2])
@NLparameter(mpc,z3in == zs[3])
@NLparameter(mpc,z4in == zs[4])
@NLparameter(mpc,z5in == zs[5])
@NLparameter(mpc,z6in == zs[6])
@NLparameter(mpc,z1sp == zs[1])
@NLparameter(mpc,z2sp == zs[2])
@NLparameter(mpc,μsp == zs[4]/zs[3])
@NLparameter(mpc,z5sp == zs[5])
@NLparameter(mpc,z6sp == zs[6])
@NLparameter(mpc,u1sp  == u1s[1])
@NLparameter(mpc,u2sp  == u2s[1])

# ================  Auxiliary expressions ============
# - MWD
@NLexpression(mpc,μm[i in n1i,j in mj],z4[i,j]/z3[i,j])

# - P0
@NLexpression(mpc,P0[i in n1i,j in mj],
   (2*fs*z2[i,j]*Zi*exp(-Ei/R/z5[i,j])/(Ztd*exp(-Etd/R/z5[i,j])
                                     +Ztc*exp(-Etc/R/z5[i,j])))^(0.5))

# - Deviation from the steady state
@NLexpression(mpc,SS[i in n1i,j in mj],Q1*(z1sp-z1[i,j])^2 
  + Q2*(z2sp-z2[i,j])^2 + Q3*(μsp-μm[i,j])^2 
  + Q5*(z5sp-z5[i,j])^2 + Q6*(z6sp-z6[i,j])^2 + Q7*(u1sp-u1[i,j])^2 
                                                  + Q8*(u2sp-u2[i,j])^2)

# - RHS of the state equations
@NLexpression(mpc,f1[i in n1i,j in mj],
 -(Zp*exp(-Ep/R/z5[i,j])+Zfm*exp(-Efm/R/z5[i,j]))*z1[i,j]*P0[i,j] + qf*(z1i0-z1[i,j])/V)

@NLexpression(mpc,f2[i in n1i,j in mj],-Zi*exp(-Ei/R/z5[i,j])*z2[i,j] 
                                             + (u1[i,j]*z2i0-qf*z2[i,j])/V)

@NLexpression(mpc,f3[i in n1i,j in mj],
   (0.5*Ztc*exp(-Etc/R/z5[i,j])+Ztd*exp(-Etd/R/z5[i,j]))*P0[i,j]^2 
      + Zfm*exp(-Efm/R/z5[i,j])*z1[i,j]*P0[i,j] - qf*z3[i,j]/V)

@NLexpression(mpc,f4[i in n1i,j in mj],
   Mm*(Zp*exp(-Ep/R/z5[i,j])+Zfm*exp(-Efm/R/z5[i,j]))*z1[i,j]*P0[i,j] - qf*z4[i,j]/V)

@NLexpression(mpc,f5[i in n1i,j in mj],P0[i,j]*Zp*exp(-Ep/R/z5[i,j])*z1[i,j]*ΔH/ρ1/cp
   - U*A*(z5[i,j]-z6[i,j])/ρ1/cp/V + qf*(z5i0 - z5[i,j])/V)

@NLexpression(mpc,f6[i in n1i,j in mj],u2[i,j]*(Tw0-z6[i,j])/V0 
                                            + U*A*(z5[i,j]-z6[i,j])/ρ2/cw/V0)

# - Value of u(t) at the beginning of element i
@NLexpression(mpc,u10[i in n1i],sum(lag[j]*u1[i,j] for j in mj))
@NLexpression(mpc,u20[i in n1i],sum(lag[j]*u2[i,j] for j in mj))

# - Collocation equations
@NLexpression(mpc,z1dot[i in n1i,j in mj],f1[i,j])
@NLexpression(mpc,z2dot[i in n1i,j in mj],f2[i,j])
@NLexpression(mpc,z3dot[i in n1i,j in mj],f3[i,j])
@NLexpression(mpc,z4dot[i in n1i,j in mj],f4[i,j])
@NLexpression(mpc,z5dot[i in n1i,j in mj],f5[i,j])
@NLexpression(mpc,z6dot[i in n1i,j in mj],f6[i,j])

# Objective function
@NLobjective(mpc,Min,sum(h1[i]*ω[j]*SS[i,j] for i in 1:(N1-1), j in mj)
 + R1*(z1[N1,M]-z1sp)^2 + R2*(z2[N1,M]-z2sp)^2 + R3*(μm[N1,M]-μsp)^2 
 + R5*(z5[N1,M]-z5sp)^2 + R6*(z6[N1,M]-z6sp)^2)

# Constraints
# - RK representation of the state and adjoint equations
@NLconstraint(mpc,g1[i in n1i,j in mj],
      z1[i,j] == z10[i]+h1[i]*sum(Ω[j,k]*z1dot[i,k] for k in mk))
@NLconstraint(mpc,g2[i in n1i,j in mj],
      z2[i,j] == z20[i]+h1[i]*sum(Ω[j,k]*z2dot[i,k] for k in mk))
@NLconstraint(mpc,g3[i in n1i,j in mj],
      z3[i,j] == z30[i]+h1[i]*sum(Ω[j,k]*z3dot[i,k] for k in mk))
@NLconstraint(mpc,g4[i in n1i,j in mj],
      z4[i,j] == z40[i]+h1[i]*sum(Ω[j,k]*z4dot[i,k] for k in mk))
@NLconstraint(mpc,g5[i in n1i,j in mj],
      z5[i,j] == z50[i]+h1[i]*sum(Ω[j,k]*z5dot[i,k] for k in mk))
@NLconstraint(mpc,g6[i in n1i,j in mj],
      z6[i,j] == z60[i]+h1[i]*sum(Ω[j,k]*z6dot[i,k] for k in mk))

# - Continuity
@NLconstraint(mpc,c1[i in 2:N1],z10[i] == z1[i-1,M])
@NLconstraint(mpc,c2[i in 2:N1],z20[i] == z2[i-1,M])
@NLconstraint(mpc,c3[i in 2:N1],z30[i] == z3[i-1,M])
@NLconstraint(mpc,c4[i in 2:N1],z40[i] == z4[i-1,M])
@NLconstraint(mpc,c5[i in 2:N1],z50[i] == z5[i-1,M])
@NLconstraint(mpc,c6[i in 2:N1],z60[i] == z6[i-1,M])

# - Boundary values
@NLconstraint(mpc,z1iv,z10[1] == z1in)
@NLconstraint(mpc,z2iv,z20[1] == z2in)
@NLconstraint(mpc,z3iv,z30[1] == z3in)
@NLconstraint(mpc,z4iv,z40[1] == z4in)
@NLconstraint(mpc,z5iv,z50[1] == z5in)
@NLconstraint(mpc,z6iv,z60[1] == z6in)

# - Low order representation for u
@NLconstraint(mpc,u1c[i in n1i,j in mj;j!=1],u1[i,1] == u1[i,j])
@NLconstraint(mpc,u2c[i in n1i,j in mj;j!=1],u2[i,1] == u2[i,j])