#  This Julia file generates the plots with the results obtained
#   by executing the main file

using PyPlot

close("all")
# Time vector
t = Array{Float64}(undef,N2*M)
for i in 1:N2
	for j in 1:M
      if i == 1
         tpre = 0
      else
         tpre = t[M*(i-1)]
      end
      t[j+M*(i-1)] = τ[j]*h2[i] + tpre
	end
end

# ========= The values at the beginning of each finite element are added ========
# - New Arrays to allocate the values
U1s  = Array{Float64}(undef,N2*M+N2)
U2s  = Array{Float64}(undef,N2*M+N2)
Z1s = Array{Float64}(undef,N2*M+N2)
Z2s = Array{Float64}(undef,N2*M+N2)
Z3s = Array{Float64}(undef,N2*M+N2)
Z4s = Array{Float64}(undef,N2*M+N2)
Z5s = Array{Float64}(undef,N2*M+N2)
Z6s = Array{Float64}(undef,N2*M+N2)
Ts  = Array{Float64}(undef,N2*M+N2-1)
m1s = Array{Float64}(undef,N2*M+N2)
m2s = Array{Float64}(undef,N2*M+N2)
m3s = Array{Float64}(undef,N2*M+N2)
m4s = Array{Float64}(undef,N2*M+N2)

# Continuity on t
for i in 2:N2
   Ts[(i-1)*M+(i-1)] = t[(i-1)*M]
end
for i in 1:N2
   for j in mj
      Ts[(M+1)*(i-1)+j] = t[M*(i-1)+j]
   end
end
pushfirst!(Ts,0.0)

# - The values at the beginning of each finite element are allocated first
for i in 1:N2
   U1s[(i-1)*M+i] = uk[(i-1)*M+1,1]
   U2s[(i-1)*M+i] = uk[(i-1)*M+1,2]
   Z1s[(i-1)*M+i] = Z0k[i,1]
   Z2s[(i-1)*M+i] = Z0k[i,2]
   Z3s[(i-1)*M+i] = Z0k[i,3]
   Z4s[(i-1)*M+i] = Z0k[i,4]
   Z5s[(i-1)*M+i] = Z0k[i,5]
   Z6s[(i-1)*M+i] = Z0k[i,6]
   m1s[(i-1)*M+i] = M0k[i,1]
   m2s[(i-1)*M+i] = M0k[i,2]
   m3s[(i-1)*M+i] = M0k[i,3]
   m4s[(i-1)*M+i] = M0k[i,4]
end

# - then the values at the all collocation points are added
for i in 1:N2
   for j in mj
      U1s[M*(i-1)+i+j]  = uk[M*(i-1)+j,1]
      U2s[M*(i-1)+i+j]  = uk[M*(i-1)+j,2]
      Z1s[M*(i-1)+i+j] = Zk[M*(i-1)+j,1]
      Z2s[M*(i-1)+i+j] = Zk[M*(i-1)+j,2]
      Z3s[M*(i-1)+i+j] = Zk[M*(i-1)+j,3]
      Z4s[M*(i-1)+i+j] = Zk[M*(i-1)+j,4]
      Z5s[M*(i-1)+i+j] = Zk[M*(i-1)+j,5]
      Z6s[M*(i-1)+i+j] = Zk[M*(i-1)+j,6]
      m1s[M*(i-1)+i+j] = Mk[M*(i-1)+j,1]
      m2s[M*(i-1)+i+j] = Mk[M*(i-1)+j,2]
      m3s[M*(i-1)+i+j] = Mk[M*(i-1)+j,3]
      m4s[M*(i-1)+i+j] = Mk[M*(i-1)+j,4]
   end
end
# ================================================================

# Plots from 0 to the point when the final time was reached
ft = Ks[1]*M + Ks[1]

μS = Z4s ./ Z3s

plt1 = subplots()
subplot(321)
plot(Ts[1:ft],μS[1:ft],linewidth=1.5)
xlabel(L"t",fontsize=12)
ylabel(L"\mu",fontsize=12)
ax = gca()
setp(ax[:get_yticklabels](),fontsize=12)
setp(ax[:get_xticklabels](),fontsize=12)

subplot(322)
plot(Ts[1:ft],Z5s[1:ft],linewidth=1.5)
xlabel(L"t",fontsize=12)
ylabel(L"T",fontsize=12)
ax = gca()
setp(ax[:get_yticklabels](),fontsize=12)
setp(ax[:get_xticklabels](),fontsize=12)

subplot(323)
plot(Ts[1:ft],U1s[1:ft],linewidth=1.5)
xlabel(L"t",fontsize=12)
ylabel(L"F_I",fontsize=12)
axhline(y=u1U,xmin=0,linewidth=1.5,linestyle="--")
axhline(y=u1L,xmin=0,linewidth=1.5,linestyle="--")
ax = gca()
setp(ax[:get_yticklabels](),fontsize=12)
setp(ax[:get_xticklabels](),fontsize=12)

subplot(324)
plot(Ts[1:ft],U2s[1:ft],linewidth=1.5)
xlabel(L"t",fontsize=12)
ylabel(L"F_{cw}",fontsize=12)
axhline(y=u2U,xmin=0,linewidth=1.5,linestyle="--")
axhline(y=u2L,xmin=0,linewidth=1.5,linestyle="--")
ax = gca()
setp(ax[:get_yticklabels](),fontsize=12)
setp(ax[:get_xticklabels](),fontsize=12)

subplot(325)
plot(Ts[1:ft],m1s[1:ft],linewidth=1.5)
plot(Ts[1:ft],m2s[1:ft],linewidth=1.5,linestyle="--")
plot(Ts[1:ft],m3s[1:ft],linewidth=1.5,linestyle=":")
plot(Ts[1:ft],m4s[1:ft],linewidth=1.5,linestyle="-.")
xlabel(L"t",fontsize=12)
ylabel(L"m^3",fontsize=12)
legend([L"I_1",L"I_2",L"I_3",L"I_4"],fontsize=12)
ax = gca()
setp(ax[:get_yticklabels](),fontsize=12)
setp(ax[:get_xticklabels](),fontsize=12)

tight_layout()
savefig("Solution.pdf")
