# This julia file generates initial guess values for z,u and y based on
# the sequence Seq0 

Seqi = Dict{Int64,Int64}()
for p in np
   Seqi[p] = Seq0[p]
end

Seqj = sort(collect(Seqi),by=x->x[1])

x10 = Array{Float64}(undef,N0,M)
x20 = Array{Float64}(undef,N0,M)
x30 = Array{Float64}(undef,N0,M)
x40 = Array{Float64}(undef,N0,M)
x50 = Array{Float64}(undef,N0,M)
x60 = Array{Float64}(undef,N0,M)
U10 = Array{Float64}(undef,N0,M)
U20 = Array{Float64}(undef,N0,M)
for q in nq
   k = Seqj[q][2]
   x10[Ni[q],:] .= z1p[k]
   x20[Ni[q],:] .= z2p[k]
   x30[Ni[q],:] .= z3p[k]
   x40[Ni[q],:] .= z4p[k]
   x50[Ni[q],:] .= z5p[k]
   x60[Ni[q],:] .= z6p[k]
   U10[Ni[q],:] .= u1p[k]
   U20[Ni[q],:] .= u2p[k]
end

M0 = Array{Float64}(undef,P,N0,M)
for p in np
   M0[p,:,:] .= msm[p]
end
for q in nq
   k = Seqj[q][2]
   M0[k,Ni[q][1]:end,:] .= D[k]
end   

y0 = Array{Float64}(undef,P,Q)
y0 .= -1.0
for q in nq
   k = Seqj[q][2]
   y0[k,q] = 1.0
end  


